import csv
import json

class Menteeslist:
    def __init__(self, filename):
        self.filename = filename
        self.mentees = []
        self.load_mentees()
        
    def load_mentees(self):
        with open(self.filename, 'r') as file:
            reader = csv.reader(file)
            next(file)
            for row in reader:
                index, first_name, last_name, language = row
                self.mentees.append({'index': index, 'first_name': first_name, 'last_name': last_name, 'language': language})

    def __str__(self):
        return str(self.mentees)

    def get_number_of_mentees(self):
        return len(self.mentees)

    def get_languages(self):
        language_set = set()
        for mentee in self.mentees:
            if mentee['language'] not in language_set:
                language_set.add(mentee['language'])
        return language_set

    def get_average_full_name_lenght(self):
        overall_names_length = 0
        for mentee in self.mentees:
            mentee_name_length = len(f'{mentee["first_name"]} {mentee["last_name"]}')
            overall_names_length += mentee_name_length
        return overall_names_length / len(self.mentees)
    
    def get_longest_name(self):
        longest_name_length = 0
        for mentee in self.mentees:
            mentee_name_length = len(f'{mentee["first_name"]} {mentee["last_name"]}')
            if mentee_name_length > longest_name_length:
                longest_name_length = mentee_name_length
                if mentee_name_length == longest_name_length:
                    mentee_with_longest_name = mentee
        return mentee_with_longest_name
    
    def get_shortest_name(self):
        shortest_name_length = 300
        for mentee in self.mentees:
            mentee_name_length = len(f'{mentee["first_name"]} {mentee["last_name"]}')
            if mentee_name_length < shortest_name_length:
                shortest_name_length = mentee_name_length
                if mentee_name_length == shortest_name_length:
                    mentee_with_shortest_name = mentee
        return mentee_with_shortest_name
    
    def generate_report(self):
        report = {
                    'All languages mentees know': list(self.get_languages()), 
                    'Number of mentees': self.get_number_of_mentees(),
                    'Average length of mentees full names': self.get_average_full_name_lenght(),
                    'Mentee with longest full name': self.get_longest_name(),
                    'Mentee with shortest full name': self.get_shortest_name()
                }
        output_filename = self.filename.split('.')[0] + '.json'
        with open(output_filename, 'w', encoding='utf-8') as of:
                json.dump(report, of, indent=4, ensure_ascii=False)

    def run_tests(self):
        assert self.get_number_of_mentees() == 100, f"Found {self.get_number_of_mentees()}"
        assert self.get_languages() == {
                                            "Slovak",
                                            "German",
                                            "Polish",
                                            "Romanian",
                                            "Czech",
                                            "English",
                                            "Spanish"
                                        }, f"Found {self.get_languages()}"
        assert self.get_average_full_name_lenght() == 13.19, f"Found {self.get_average_full_name_lenght()}"
        assert self.get_longest_name() == {
                                            "index": "65",
                                            "first_name": "Javontena",
                                            "last_name": "Delossantos",
                                            "language": "German"
                                            }, f"Found {self.get_longest_name()}"        
        assert self.get_shortest_name() == {
                                            "index": "18",
                                            "first_name": "Sana",
                                            "last_name": "Van",
                                            "language": "English"
                                            }, f"Found {self.get_shortest_name()}"
        
Mentees = Menteeslist('Mentee list - Sheet1.csv')